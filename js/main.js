'use strict;';

var elements = {
    adoption: document.getElementById('adoption-table'),
    totals: document.getElementById('totals'),
};

/**
 * Refreshes all the data in the page
 */
function refresh() {
    reqwest({
        url: 'https://ttsda.cc/btc/nodes.json'
    })
    .then(function(data) {
        var meta = {
            typeCounts: {},
            countryCounts: {}
        };

        var nodes = [];

        // process the raw data
        _.each(data.nodes, function(row) {
            var node = {
                version: row[0],
                userAgent: row[1],
                connectedSince: row[2],
                services: row[3],
                height: row[4],
                hostname: row[5],
                city: row[6],
                country: row[7],
                latitude: row[8],
                longitude: row[9],
                timezone: row[10],
                asn: row[11],
                organization: row[12]
            };

            var uaRegex = /\/(.+?):(.+?)\/?/;
            var uaSplit = uaRegex.exec(node.userAgent);

            if (uaSplit) {
                node.clientType = uaSplit[1];
                node.clientVersion = uaSplit[2];
            }
            else {
                node.clientType = null;
                node.clientVersion = null;
            }

            // Rename "Satoshi" to "Core"
            if (node.clientType == 'Satoshi')
                node.clientType = 'Core';

            nodes.push(node);

            // increment the per-type count
            meta.typeCounts[node.clientType] = (meta.typeCounts[node.clientType] || 0) + 1;

            // increment the per-country per-type count
            meta.countryCounts[node.country] = meta.countryCounts[node.country] || {};
            meta.countryCounts[node.country][node.clientType] = (meta.countryCounts[node.country][node.clientType] || 0) + 1;
        });

        // display the data
        displayAdoption(meta);
        displayTotals(meta);
        displayMap(meta);
    });
}

function displayTotals(meta) {
    var dl = replace(elements.totals, document.createElement('dl'));

    var totalData = _.chain(meta.typeCounts)
        .map(function(count, type) {
            return {count: count, type: type};
        })
        .filter(function(client) {
            return client.count > 10;
        })
        .sortBy('count')
        .reverse()
        .value();

    _.each(totalData, function(client) {
        appendDefinition(dl, client.type, client.count);
    });
}

function displayMap(meta) {
    var data = {};

    _.each(meta.countryCounts, function(countrydata, code) {
        if (!countryCodes[code])
            return;

        var countryobj = data[countryCodes[code].alpha3] = {};

        countryobj.counts = _.chain(countrydata)
            .map(function(count, type) {
                return {
                    type: type,
                    count: count,
                };
            })
            .sortBy('count')
            .reverse()
            .value();

        countryobj.fillKey = _.max(countryobj.counts, function(client) { return client.count; }).type;
    });

    var otherColors = ["#90a959", "#aa759f", "#ac4142"];
    var fills = {
        defaultFill: "lightgray",
        Classic: "#6a9fb5",
        Core: "#d28445"
    };
    var pickedColors = 0;
    _.each(data, function(datapoint) {
        if (fills[datapoint.fillKey] === undefined) {
            fills[datapoint.fillKey] = otherColors[pickedColors];
            pickedColors++;
        }
    });


    var map = new Datamap({
        element: document.getElementById('map'),
        projection: 'mercator',
        height: 700,
        width: 900,
        geographyConfig: {
            popupTemplate: function(geography, data) {
                var counts =_.map(data.counts.slice(0,5), function(count) {
                    return '<b>' + count.type + '</b>: ' + count.count + '<br>';
                }).join('');
                return '<div class="hoverinfo"><b>' + geography.properties.name + '</b><hr>' + counts + '</div>';
            },
        },
        fills: fills,
        data: data
    });
}

/**
 * Displays the adoption rate table data
 * @param  {Object} meta The metadata processed on refresh()
 */
function displayAdoption(meta) {
    // create a table
    var table = replace(elements.adoption, document.createElement('table'));

    var thead = document.createElement('thead');
    var tbody = document.createElement('tbody');
    table.appendChild(thead);
    table.appendChild(tbody);

    // process the data for each client type
    var totalData = _.chain(meta.typeCounts)
        .map(function(count, type) {
            return {count: count, type: type};
        })
        .filter(function(client) {
            return client.count > 30;
        })
        .sortBy('count')
        .reverse()
        .value();

    // process the data for each country
    var countryData = _.sortBy(
        // map each country into an object {country, {type: count}}
        _.map(meta.countryCounts, function(counts, country) {
            return {
                code: country,
                name: countryCodes[country] ? countryCodes[country].name : 'N/A',
                counts: counts,
                sum: _.reduce(_.values(counts), function(a, b) {return a + b;})
            };
        }),

        // sort countries by the amount of nodes...
        function(country) {
            return _.chain(country.counts)
                .reduce(function(a, b) { return a + b; })
                .value();
        }
    ).reverse(); // ...in descending order

    // display the data
    appendRow(thead, ['Country'].concat(_.pluck(totalData, 'type')), {header: true});

    _.each(countryData, function(country) {
        var textValues = [];
        var percentages = [];

        _.each(totalData, function(client) {
            var count = country.counts[client.type] || 0;
            var percentage = Math.round(count / country.sum * 1000) / 10;

            textValues.push(count + " (" + percentage + "%)");
            percentages.push(percentage);
        });

        appendRow(tbody, [country.name].concat(textValues), {realValues: [undefined].concat(percentages)});
    });

    // show the table
    table.dataset.sortable = true;
    table.className = "sortable-theme-light";
    Sortable.init(table);

    // sort by Classic
    sortableSortBy(table, 'Classic');
}

/**
 * Appends a row to a table
 * @param  {Element} table              A table element
 * @param  {(string|number)[]} values   A list of values
 * @param  {Object} options
 * @param  {bool}   options.header                  Set to true to write th instead of td
 * @param  {(string|number)[]} options.realValues   The values to write to the data-value parameter on each td
 */
function appendRow(table, values, options) {
    options = options || {};

    var tr = document.createElement('tr');
    _.each(values, function(value, i) {
        var td = document.createElement(options.header === true ? 'th' : 'td');

        if (options.realValues !== undefined) {
            if (options.realValues[i])
                td.dataset.value = options.realValues[i];
        }

        td.innerHTML = value;
        tr.appendChild(td);
    });
    table.appendChild(tr);
}

/**
 * Sorts a sortable table by a certain row
 * @param  {Element} table A table
 * @param  {string}  row   The row title
 */
function sortableSortBy(table, row) {
    var headers = document.querySelectorAll('th');

    _.each(headers, function(header) {
        if (header.innerHTML == row)
            header.click();
    });
}

/**
 * Appends a definition to a definition list
 * @param  {Element} dl    Definiton list
 * @param  {string}  key
 * @param  {string}  value
 */
function appendDefinition(dl, key, value) {
    var dt = document.createElement('dt');
    var dd = document.createElement('dd');

    dt.innerHTML = key;
    dd.innerHTML = value;

    dl.appendChild(dt);
    dl.appendChild(dd);
}

/**
 * Replaces the inside of an element with another element
 * @param  {Element} a  Element to clear
 * @param  {Element} b  Element to fill the cleared element
 * @return {Element}    Element 'b'
 */
function replace(a, b) {
    a.innerHTML = "";
    a.appendChild(b);
    return b;
}

/**
 * Loads click events to all the tab links in the page, and opens the default tab.
 */
function loadTabs() {
    var tabsLists = document.querySelectorAll('[data-tabs]');
    var defaultTab;

    _.each(tabsLists, function(tabsList) {
        var links = tabsList.querySelectorAll('a');

        _.each(links, function(link) {
            var tab = link.hash.substring(1);

            link.addEventListener('click', function() {
                openTab(tab);
            });

            if (link.dataset.default !== undefined)
                defaultTab = tab;
        });
    });

    if (location.hash !== "")
    {
        var hashTab = location.hash.substring(1);
        if (document.querySelector('[data-tab=' + hashTab + ']') !== null)
            defaultTab = hashTab;
    }

    openTab(defaultTab);
}

/**
 * Shows a tab (and hides every other tab)
 * @param  {string} tabName The stub name of the tab
 */
function openTab(tabName) {
    var tabs = document.querySelectorAll('[data-tab]');

    _.each(tabs, function(tab) {

        if (tab.dataset.tab == tabName) {
            if (tab.style.display == 'none')
                tab.style.display = tab.dataset.realDisplay || 'initial';
        }
        else {
            if (tab.style.display != 'none')
            {
                tab.dataset.realDisplay = tab.style.display;
                tab.style.display = 'none';
            }
        }
    });
}

loadTabs();
refresh();
